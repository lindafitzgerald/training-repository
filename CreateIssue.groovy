import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.util.JiraUtils

import org.apache.log4j.Logger
import org.apache.log4j.Level

def log = Logger.getLogger("com.acme.createIssue")
log.setLevel(Level.DEBUG)

def changeHolder = new DefaultIssueChangeHolder()
def issueManager = ComponentAccessor.getIssueManager()
def issueFactory = ComponentAccessor.getIssueFactory()
def cfManager = ComponentAccessor.getCustomFieldManager()
def linkAstonaut = ComponentAccessor.getIssueLinkManager()


//Issue issue = ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase("CRM-11628")

def tisProject = ComponentAccessor.getProjectManager().getProjectObj(10100) //1
ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
myuser = ComponentAccessor.getUserUtil().getUserByName("linda.fitzgerald")
ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(myuser)

def currentUser = ComponentAccessor.getJiraAuthenticationContext().getUser()

//def cFieldsFiltered = cfManager.getCustomFieldObjects(issue).findAll { issue.getCustomFieldValue(it) } as List<CustomField>


def assignee = issue.getAssignee()
def summary = "Astronaut"

MutableIssue newAstronaut = issueFactory.getIssue()
newAstronaut.setSummary(summary)
newAstronaut.setProjectObject(myuser)
newAstronaut.setIssueTypeId("10400")

//cFieldsFiltered.each { field -> newAstronaut.setCustomFieldValue(field, issue.getCustomFieldValue(field)) }


newAstronaut.setAssignee(user)
newAstronaut.setReporter(user)

//newMeasuring.setCustomFieldValue(mailBodyField,"")
issueManager.updateIssue(myuser, newAstronaut, EventDispatchOption.DO_NOT_DISPATCH, false)
linkAstonaut.createIssueLink(newAstronaut.id, issue.id, 10200, 1, myuser)